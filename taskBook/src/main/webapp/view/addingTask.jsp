<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adding Task</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="../res/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../res/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <script src="../res/js/jquery-3.4.1.min.js"></script>
    <script src="../res/js/jquery-ui.min.js"></script>

    <script>
        $(function () {
            $("#startDate").datepicker();
        });
    </script>

    <script>
        $(function () {
            $("#endDate").datepicker();
        });
    </script>

</head>
<body style="font-family: 'Roboto', sans-serif;">

<h1>Task Book</h1>

<hr>

<form class="addingTask" action='findTask' method="get">


    <div class="field">
        <label for="assignee">Assignee:</label>
        <input id="assignee" name='assignee' placeholder="Frodo Baggins">
    </div>

    <div class="field">
        <label for="startDate">Start Date:</label>
        <input id="startDate" name="startDate" placeholder="04/01/2019">
    </div>

    <div class="field">
        <label for="endDate">End Date:</label>
        <input id="endDate" name="endDate" placeholder="04/01/2020">
    </div>

    <div class="field">
        <label for="summary">Summary:</label>
        <textarea rows="6" cols="55" maxlength="255" id="summary"
                  name='summary' placeholder="Bring the One Ring"></textarea>
    </div>

    <br>

    <div>
        <button>Add new Task</button>
    </div>

    <hr>
    <%
        String checkData = (String) request.getAttribute("checkData");

        if (checkData != null) {
            switch (checkData) {
                case "someEmpty":
    %>

    <div>
        <h4>Some field is empty. Recheck please.</h4>
    </div>

    <%
            break;
        case "wrongPeriodOfDate":
    %>

    <div>
        <h4> You set wrong period of time. Start day must be earlier then end day.</h4>
    </div>

    <%
                    break;
            }
        }
    %>

</form>

</body>
</html>
