package com.vmart.servlet;

import com.vmart.service.Domain;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class FindingTaskServlet extends HttpServlet {

    private Domain domain;

    @Override
    public void init() {
        domain = new Domain();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){

        String link = domain.checkInputData(req);

        if (req.getAttribute("checkData").equals("ok")) {
            domain.createTask(req);
            domain.getAllAssignee(req);
        }
        try {
            req.getRequestDispatcher(link).forward(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){

        domain.find(req);
        domain.getAllAssignee(req);

        try {
            req.getRequestDispatcher("view/findingTask.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
