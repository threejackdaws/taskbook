package com.vmart.DAO.impl;

import com.vmart.DAO.DAO;
import com.vmart.entitie.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;


public class TaskDAO implements DAO<Task, String> {

    private Connection connection;

    public TaskDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Данный вид вставки данных в таблицу позволяет избежать
     * полного дубликатирования данных пользователем.
     */
    @Override
    public boolean merge(Task model) {
        boolean result = false;

        String sql = "MERGE INTO tasks (summary, assignee, start_date, end_date)  KEY (summary, assignee, start_date, end_date) " +
                "VALUES (?, ?, ?, ?);";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getSummary());
            statement.setString(2, model.getAssignee());
            statement.setObject(3, model.getStartDate());
            statement.setObject(4, model.getEndDate());
            result = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public boolean insert(Task model) {
        boolean result = false;

        String sql = "INSERT INTO tasks (summary, assignee, start_date, end_date) " +
                "VALUES (?, ?, ?, ?);";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getSummary());
            statement.setString(2, model.getAssignee());
            statement.setObject(3, model.getStartDate());
            statement.setObject(4, model.getEndDate());
            result = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Task read(Long taskId) {
        final Task result = new Task();
        result.setId(-1);

        String sql = "SELECT * FROM tasks WHERE id = (?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, taskId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result.setId(resultSet.getLong("id"));
                result.setAssignee(resultSet.getString("assignee"));
                result.setSummary(resultSet.getString("summary"));
                result.setStartDate(resultSet.getDate("startDate").toLocalDate());
                result.setEndDate(resultSet.getDate("endDate").toLocalDate());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void update(Task model) {

        String sql = "UPDATE tasks SET" +
                " assignee = (?)," +
                " summary = (?)," +
                " start_date = (?)," +
                "end_date = (?)" +
                "WHERE id = (?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getAssignee());
            statement.setString(2, model.getSummary());
            statement.setObject(3, model.getStartDate());
            statement.setObject(4, model.getEndDate());
            statement.setLong(5, model.getId());
            statement.executeQuery().next();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean delete(Task model) {
        boolean result = false;

        String sql = "DELETE FROM tasks WHERE id = (?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, model.getId());
            result = statement.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Task> readAll() {
        List<Task> result = new ArrayList<>();

        String sql = "SELECT * FROM tasks;";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            createListWithTasks(result, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Метод позволяет выбрать из таблицы уникальных @assignee.
     */

    public Set<String> readAllAssignee() {
        Set<String> result = new HashSet<>();

        String sql = "SELECT assignee FROM tasks;";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getString("assignee"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public List<Task> readTasksWhereAssignee(String assignee) {

        List<Task> result = new ArrayList<>();

        String sql = "SELECT * FROM tasks WHERE assignee = (?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, assignee);

            ResultSet resultSet = statement.executeQuery();
            createListWithTasks(result, resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public List<Task> readTasksWherePeriod(LocalDate firstDate, LocalDate secondDate) {

        List<Task> result = new ArrayList<>();

        String sql = "SELECT * FROM tasks WHERE START_DATE >= (?) and END_DATE <= (?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setObject(1, firstDate);
            statement.setObject(2, secondDate);

            ResultSet resultSet = statement.executeQuery();
            createListWithTasks(result, resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Task> readTasksWherePeriodAndAssignee(String assignee, LocalDate firstDate, LocalDate secondDate) {

        List<Task> result = new ArrayList<>();

        String sql = "SELECT * FROM tasks WHERE START_DATE >= (?) and END_DATE <= (?) and ASSIGNEE = (?);";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setObject(1, firstDate);
            statement.setObject(2, secondDate);
            statement.setString(3, assignee);

            ResultSet resultSet = statement.executeQuery();
            createListWithTasks(result, resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Мето позволяет избежать дубликатирования кода.
     */
    private void createListWithTasks(List<Task> result, ResultSet resultSet) throws SQLException {

        while (resultSet.next()) {
            Task task = new Task();
            task.setId(resultSet.getLong("id"));
            task.setSummary(resultSet.getString("summary"));
            task.setAssignee(resultSet.getString("assignee"));
            task.setStartDate(resultSet.getDate("start_date").toLocalDate());
            task.setEndDate(resultSet.getDate("end_date").toLocalDate());

            result.add(task);
        }
    }

}
