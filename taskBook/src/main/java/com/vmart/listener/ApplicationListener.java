package com.vmart.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


/**
Запускается один раз на старте приложения, создает таблицу в базе данных.
*/

@WebListener()
public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:h2:tcp://localhost/~/test;DB_CLOSE_DELAY=-1",
                    "sa",
                    ""
            );

            Statement statement = conn.createStatement();

            String createTacksDataBase =
                    "CREATE TABLE tasks ( " +
                            "id INT NOT NULL auto_increment," +
                            "summary VARCHAR(255) NOT NULL ," +
                            "start_date DATE ," +
                            "end_date DATE, " +
                            "assignee VARCHAR (50)" +
                            ");";

            statement.execute(createTacksDataBase);

            statement.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}