package com.vmart.service;

import com.vmart.entitie.Task;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;


public interface TaskService {


    void createTask(HttpServletRequest req);

    List<Task> getAll();

    Set<String> getAllAssignee();

    List<Task> findTasksByAssignee(String assignee);

    List<Task> findTasksByStartAndEndDates(LocalDate firstDate, LocalDate secondDate);

    boolean checkPeriodOfDate(HttpServletRequest req);

    List<Task> findTaskByPeriodAndByAssignee(String assignee, LocalDate firstDate, LocalDate secondDate);
}
