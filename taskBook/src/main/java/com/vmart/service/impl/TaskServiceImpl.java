package com.vmart.service.impl;

import com.vmart.DAO.ConnectionToDB;
import com.vmart.DAO.impl.TaskDAO;
import com.vmart.entitie.Task;
import com.vmart.service.TaskService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;


public class TaskServiceImpl implements TaskService {

    private TaskDAO taskDAO;


    @Override
    public void createTask(HttpServletRequest req) {
        try (Connection connection = ConnectionToDB.openConnection()) {
            taskDAO = new TaskDAO(connection);

            String summary = req.getParameter("summary");
            String assignee = req.getParameter("assignee");

            String startDateBeforePreparation = req.getParameter("startDate");
            String endDateBeforePreparation = req.getParameter("endDate");

            LocalDate startDate = preparation(startDateBeforePreparation);
            LocalDate endDate = preparation(endDateBeforePreparation);

            Task task = new Task(summary, assignee, startDate, endDate);
            taskDAO.merge(task);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Подготавливает входные значени для записи в базу данных в
     * требуемом формате.
     */
    private LocalDate preparation(String dateBeforePreparation) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        LocalDate date = LocalDate.parse(dateBeforePreparation, formatter);
        return date;
    }

    @Override
    public List<Task> getAll() {
        List<Task> result = null;
        try (Connection connection = ConnectionToDB.openConnection()) {
            taskDAO = new TaskDAO(connection);
            result = taskDAO.readAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Set<String> getAllAssignee() {
        Set<String> result = null;
        try (Connection connection = ConnectionToDB.openConnection()) {
            taskDAO = new TaskDAO(connection);
            result = taskDAO.readAllAssignee();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Task> findTasksByAssignee(String assignee) {
        List<Task> result = null;
        try (Connection connection = ConnectionToDB.openConnection()) {
            taskDAO = new TaskDAO(connection);
            result = taskDAO.readTasksWhereAssignee(assignee);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Task> findTasksByStartAndEndDates(LocalDate firstDate, LocalDate secondDate) {
        List<Task> result = null;
        try (Connection connection = ConnectionToDB.openConnection()) {
            taskDAO = new TaskDAO(connection);
            result = taskDAO.readTasksWherePeriod(firstDate, secondDate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Task> findTaskByPeriodAndByAssignee(String assignee, LocalDate firstDate, LocalDate secondDate) {
        List<Task> result = null;
        try (Connection connection = ConnectionToDB.openConnection()) {
            taskDAO = new TaskDAO(connection);
            result = taskDAO.readTasksWherePeriodAndAssignee(assignee, firstDate, secondDate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @return @startDate.isAfter(endDate)
     * позволяет пользователю выставлять @Task с продолжительностью в 1 день, когда @startDate = @endDate.
     */

    @Override
    public boolean checkPeriodOfDate(HttpServletRequest req) {

        String startDateBeforePreparation = req.getParameter("startDate");
        String endDateBeforePreparation = req.getParameter("endDate");

        LocalDate startDate = preparation(startDateBeforePreparation);
        LocalDate endDate = preparation(endDateBeforePreparation);

        return startDate.isAfter(endDate);
    }
}
