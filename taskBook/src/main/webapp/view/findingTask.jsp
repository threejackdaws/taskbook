<%@ page import="com.vmart.entitie.Task" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Finding Task</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="../res/css/style.css" rel="stylesheet" type="text/css"/>
    <script src="../res/js/checkPeriod.js"></script>

</head>
<body style="font-family: 'Roboto', sans-serif;">

<h1>Find Tasks</h1>

<hr>

<form method="post">


    <div class="field">
        <label for="startDate">Start Date:</label>
        <input name='start_Date' id="startDate" placeholder="2019-01-21">
    </div>

    <div class="field">
        <label for="endDate">End Date:</label>
        <input name='end_Date' id="endDate" placeholder="2019-01-31">
    </div>


    <div class="field">
        <label for="assignee">Assignee:</label>
        <select id="assignee" name="assignees">
            <option value="">All assignees</option>

            <% Set<String> allAssignee = (Set<String>) request.getAttribute("allAssignee");
                for (String assignee : allAssignee) {
            %>

            <option value="<%=assignee%>">
                <%=assignee%>
            </option>

            <%
                }
            %>
        </select>
    </div>


    <div class="field">
        <label for="period">Period:</label>
        <select onchange="checkPeriod()" name="period" id="period">
            <option value="none">None</option>
            <option value="lastQuarter">Last Quarter</option>
            <option value="lastMonth">Last Month</option>
            <option value="lastWeek">Last Week</option>
            <option value="currentQuarterToDate">Current Quarter to Date</option>
            <option value="currentMonthToDate">Current Month to Date</option>
            <option value="currentWeekToDate">Current Week to Date</option>
        </select>
    </div>

    <br>

    <div>
        <button type="submit">Find</button>
    </div>

</form>

<div>
    <table border="1">

        <%
            List<Task> foundTasks = (ArrayList<Task>) request.getAttribute("findTask");

            if ((foundTasks != null) && (!foundTasks.isEmpty())) {
        %>

        <tr>
            <th>Id</th>
            <th>Summary</th>
            <th>StartDate</th>
            <th>EndDate</th>
            <th>Assignee</th>
        </tr>

        <%
            long id = 0;
            for (Task task : foundTasks) {

                id++;
        %>

        <tr>
            <td><%=id%>
            </td>
            <td><%=task.getSummary()%>
            </td>
            <td><%=task.getStartDate()%>
            </td>
            <td><%=task.getEndDate()%>
            </td>
            <td><%=task.getAssignee()%>
            </td>
        </tr>

        <%
                }
            }
        %>
    </table>
</div>

<br>

<form action="taskBook" method="get">

    <div>
        <button>Previous Page</button>
    </div>

</form>

<hr>

<%
    String infoAboutDate = (String) request.getAttribute("infoAboutDate");
    if (infoAboutDate != null) {

        switch (infoAboutDate) {
            case "ok":
                if (foundTasks.isEmpty()) {
%>
<div>
    <h4>There are no entries in the database for the specified search criteria. Select other criteria.</h4>
</div>
<%
        }
        break;

    case "wrongInputStartDate":
%>
<div>
    <h4>Wrong input date in field StartDate. It must be format yyyy-MM-dd</h4>
</div>
<%
        break;

    case "wrongInputEndDate":
%>
<div>
    <h4>Wrong input date in field EndDate. It must be format yyyy-MM-dd</h4>
</div>
<%
        break;

    case "needToSelectSearchCriteria":
%>
<div>
    <h4>You should select your search criteria.</h4>
</div>
<%
         break;

        }
    }
%>


</body>
</html>
