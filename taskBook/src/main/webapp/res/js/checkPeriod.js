
function checkPeriod() {

    let element = document.getElementById("period");
    let period = element.options[element.selectedIndex].value;

    let today = new Date();

    let monthNow = today.getMonth();
    let yearNow = today.getFullYear();

    let quarter = Math.floor((today.getMonth() + 3) / 3);

    function parseDate(date) {

        let yyyy = date.getFullYear();
        let day = date.getDate();
        let month = date.getMonth();

        let dd = String(day).padStart(2, "0");
        let mm = String(month + 1).padStart(2, "0");

        day = yyyy + '-' + mm + '-' + dd;
        return day;
    }

    switch (period) {

        case "lastQuarter":

            let firstDayLastQuarter;
            if (quarter == 1) {
                firstDayLastQuarter = new Date(today.getFullYear() - 1, 9, 1);
            } else {
                firstDayLastQuarter = new Date(today.getFullYear(), (quarter * 3) - 6, 1);
            }

            let lastDayLastQuarter;
            if (quarter == 1) {
                lastDayLastQuarter = new Date(today.getFullYear() - 1, 11, 31);
            } else {
                lastDayLastQuarter = new Date(today.getFullYear(), (quarter * 3) - 3, 0);
            }

            document.getElementById('startDate').value = parseDate(firstDayLastQuarter);
            document.getElementById('endDate').value = parseDate(lastDayLastQuarter);
            break;

        case "lastMonth":

            let firstDayLastMonth = new Date(yearNow, monthNow - 1, 1);
            let lastDayLastMonth = new Date(yearNow, monthNow, 0);

            document.getElementById('startDate').value = parseDate(firstDayLastMonth);
            document.getElementById('endDate').value = parseDate(lastDayLastMonth);
            break;

        case "lastWeek":
            let day1 = today.getDay() || 7;
            if (day1 !== 1) {
                today.setHours(-24 * (day1 - 1));
            }

            let lYear = today.getFullYear();
            let lMonth = today.getMonth();
            let lDay = today.getDate();

            let firstDayLastWeek = new Date(lYear, lMonth, lDay - 7);
            let lastDayLastWeek = new Date(lYear, lMonth, lDay - 1);

            document.getElementById('startDate').value = parseDate(firstDayLastWeek);
            document.getElementById('endDate').value = parseDate(lastDayLastWeek);
            break;

        case "currentQuarterToDate":

            let firstDayCurrentQuarter;
            if (quarter == 1) {
                firstDayCurrentQuarter = new Date(today.getFullYear(), 1, 1);
            } else {
                firstDayCurrentQuarter = new Date(today.getFullYear(), (quarter * 3) - 3, 1);
            }

            document.getElementById('startDate').value = parseDate(firstDayCurrentQuarter);
            document.getElementById('endDate').value = parseDate(today);
            break;

        case "currentMonthToDate":

            let firstDayCurrentMonth = new Date(yearNow, monthNow, 1 );

            document.getElementById('startDate').value = parseDate(firstDayCurrentMonth);
            document.getElementById('endDate').value = parseDate(today);
            break;


        case "currentWeekToDate":

            let firstDayCurrentWeek = new Date();

            let day = firstDayCurrentWeek.getDay() || 7;
            if (day !== 1) {
                firstDayCurrentWeek.setHours(-24 * (day - 1));
            }

            document.getElementById('startDate').value = parseDate(firstDayCurrentWeek);
            document.getElementById('endDate').value = parseDate(today);
            break;
    }
}
