package com.vmart.DAO;

import java.util.List;

/**
 *Перечислены основные методы позволяющие работать с базой данных (CRUD).
 *
 * @param <Entity>
 * @param <Key>
 */

public interface DAO<Entity, Key> {

    boolean insert(Entity model);

    List<Entity> readAll();

    void update(Entity model);

    boolean delete(Entity model);

    boolean merge(Entity model);



}
