package com.vmart.service;

import com.vmart.service.impl.TaskServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.time.DateTimeException;
import java.time.LocalDate;


public class Domain {

    public void createTask(HttpServletRequest req) {
        TaskService taskService = new TaskServiceImpl();
        taskService.createTask(req);
    }


    public void getAllAssignee(HttpServletRequest req) {
        TaskService taskService = new TaskServiceImpl();
        req.setAttribute("allAssignee", taskService.getAllAssignee());
    }

    /**
     * Метод проверят какие критерии для поиска @task ввел пользователь.
     * В зависимости от этого выбирается метод для класса @TaskServiceImpl.
     */

    public void find(HttpServletRequest req) {

        TaskService taskService = new TaskServiceImpl();

        String assignee = req.getParameter("assignees");
        String startDate = req.getParameter("start_Date");
        String endDate = req.getParameter("end_Date");

        LocalDate firstDate;
        LocalDate secondDate;

        if (((!startDate.equals("")) || (!endDate.equals(""))) && (!assignee.equals(""))) {

            setAttributeAboutCorrectInputData(req);

            firstDate = checkFirstDate(startDate, req);
            secondDate = checkSecondDate(endDate, req);

            req.setAttribute("findTask", taskService.findTaskByPeriodAndByAssignee(assignee, firstDate, secondDate));
            return;
        }
        if (!assignee.equals("")) {

            setAttributeAboutCorrectInputData(req);
            req.setAttribute("findTask", taskService.findTasksByAssignee(assignee));
            return;
        }
        if ((!startDate.equals("")) || (!endDate.equals(""))) {

            setAttributeAboutCorrectInputData(req);

            firstDate = checkFirstDate(startDate, req);
            secondDate = checkSecondDate(endDate, req);

            req.setAttribute("findTask", taskService.findTasksByStartAndEndDates(firstDate, secondDate));
            return;
        }
        req.setAttribute("infoAboutDate", "needToSelectSearchCriteria");
    }


    private void setAttributeAboutCorrectInputData(HttpServletRequest req) {
        req.setAttribute("infoAboutDate", "ok");
    }


    private LocalDate checkFirstDate(String startDate, HttpServletRequest req) {

        LocalDate firstDate;
        try {
            firstDate = LocalDate.parse(startDate);
            return firstDate;
        } catch (DateTimeException e) {
            req.setAttribute("infoAboutDate", "wrongInputStartDate");
            return null;
        }
    }

    private LocalDate checkSecondDate(String endDate, HttpServletRequest req) {

        LocalDate secondDate;
        try {
            secondDate = LocalDate.parse(endDate);
            return secondDate;
        } catch (DateTimeException e) {
            req.setAttribute("infoAboutDate", "wrongInputEndDate");
            return null;
        }
    }


    /**
     * Метод проверят получаемые сервером данные от пользователя при создании @Task.
     *
     * @return String/ Возвращаемая строка представляет собой адрес JSP страницы которая будет запущена,
     * в зависимости от введенных пользователем параметров при добавлении новой @Task.
     */
    public String checkInputData(HttpServletRequest req) {

        TaskService taskService = new TaskServiceImpl();

        String summary = req.getParameter("summary");
        String assignee = req.getParameter("assignee");

        String startData = req.getParameter("startDate");
        String endData = req.getParameter("endDate");

        if ((summary == null) || (assignee == null) || (startData == null) || (endData == null)
                || (summary.equals("")) || (assignee.equals("")) || (startData.equals("")) || (endData.equals(""))) {

            req.setAttribute("checkData", "someEmpty");
            return "view/addingTask.jsp";

        } else {

            boolean periodOfDate = taskService.checkPeriodOfDate(req);

            /** Данный условие отлавливает ситуации когда пользователь задал @startDate > @endDate.
             */

            if (periodOfDate) {

                req.setAttribute("checkData", "wrongPeriodOfDate");
                return "view/addingTask.jsp";
            } else {
                req.setAttribute("checkData", "ok");
                return "view/findingTask.jsp";
            }
        }
    }
}
