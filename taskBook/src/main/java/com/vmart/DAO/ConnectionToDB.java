package com.vmart.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Позволяет создавать подклюение к базе данных и закрвать.
 * База данных H2.
 * url - jdbc:h2:tcp://localhost/~/test.
 * Имя пользователя - "sa".
 * Пароль - "".
 * База данных должна быть запущена у пользователя.
 *
*/
public class ConnectionToDB implements AutoCloseable {

    private static Connection connection;

    public static Connection openConnection() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:h2:tcp://localhost/~/test",
                    "sa",
                    ""
            );
        } catch (SQLException e){
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}
